# UnixCentric - Unix System and C (c99, c11) Development

Everything is focused on Unix and Unix-like systems.

## src
 [C](https://github.com/b3h3moth/UnixCentric/tree/master/src/C/)

Learning the art of the C language programming and the Unix Systems Programming.

## doc (Documentation)

A lot of documentation: the standards, reference manuals, bibliography 
(printed books), open/free docs, articles, talks.
