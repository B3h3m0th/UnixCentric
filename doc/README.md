# UnixCentric Documentation 

La documentazione e' suddivisa in sezioni, ciascuna delle quali e' un unico
documento in modo tale da rendere la fruibilita' il piu' veloce possibile.

UnixCentric riguarda ovviamente tutto cio' che e' inerente l'Universo UNIX, 
tuttavia, cosi' come per il codice, ci sono due aree principali, ovvero il 
linguaggio C - standard c99 e c11 - e la programmazione di sistema, che
implicitamente vanno a sfociare nell'argomento che e' il fulcro di tutto, 
ovvero l'implementazione dei sistemi operativi UNIX e UNIX-like.

* [Standards](stds_and_refs.md) `Standards, Standards de facto, Reference manuals`
* [Bibliography](biblio.md) `Printed books`
* [Books](free_books.md) `Free (full) books`
* [Articles](articles.md) `Articles and tutorial`
* [Papers and Talks](talks.md) `Papers and talks from University, Conference and so on`
* [Unix_History](unix_history) `Unix History and Timeline`
