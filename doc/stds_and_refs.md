# Standards, Standards de facto and Manual References

## International Standard for the C Programming language

- [ISO/IEC 9899:2011 - aka C11](https://dl.dropboxusercontent.com/u/57071683/draft_C11_n1570.pdf) `The third formal standard (current)`
- [ISO/IEC 9899:1999 - aka C99](https://dl.dropboxusercontent.com/u/57071683/draft_C99_n1256.pdf) `The second formal standard`
- [ISO/IEC 9899:1990 - aka C90](https://dl.dropboxusercontent.com/u/57071683/ANSI_ISO_9899-1990.pdf) `The first formal standard`

## Application Programming Interface (API) Standards - Single UNIX Specification

* [Single UNIX Specification v4](http://pubs.opengroup.org/onlinepubs/9699919799/download/susv4tc2.tgz) `Version 4, 2016 edition`
* [Single UNIX Specification v3](http://pubs.opengroup.org/onlinepubs/009695399/download/susv3.tgz) `Version 3, 2004 edition`
* [Single UNIX Specification v2](http://pubs.opengroup.org/onlinepubs/007908799/download/susv2.tgz) `Version 2, 1998 edition`

## The GNU project development resources

- [The GNU C Library](https://www.gnu.org/software/libc/manual/pdf/libc.pdf) `Last updated August 5, 2016, for glibc 2.24`
- [The GNU C Reference Manual](https://www.gnu.org/software/gnu-c-manual/gnu-c-manual.pdf) `v0.2.5 release`
- [GNU coding standards](https://www.gnu.org/prep/standards/standards.pdf) `Last updated July 25, 2016`
