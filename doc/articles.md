# Articles

## The C Programming Language

* [How is GNU `yes` so fast?](https://www.reddit.com/r/unix/comments/6gxduc/how_is_gnu_yes_so_fast/) `06/13/2017, reddit unix`
* [Yes C is unsafe, but…](https://daniel.haxx.se/blog/2017/03/30/yes-c-is-unsafe-but/) `03/30/2017, Daniel Stenberg`
* [curl is C](https://daniel.haxx.se/blog/2017/03/27/curl-is-c/) `03/27/2017, Daniel Stenberg`
* [A critique of "How to C in 2016"](https://github.com/Keith-S-Thompson/how-to-c-response) `1/15/2016, Keith Thompson`
* [How to C in 2016](https://matt.sh/howto-c) `2016, Matt Stancliff`
* [Write a Shell in C](https://brennan.io/2015/01/16/write-a-shell-in-c/) `1/16/2015, Stephen Brennan`
* What Every C Programmer Should Know About Undefined Behavior `May 2011, Chris Lattner`
    - [part 1](http://blog.llvm.org/2011/05/what-every-c-programmer-should-know.html)
    - [part 2](http://blog.llvm.org/2011/05/what-every-c-programmer-should-know_14.html)
    - [part 3](http://blog.llvm.org/2011/05/what-every-c-programmer-should-know_21.html)

### C: Memory Management

* [What a C programmer should know about memory](http://marek.vavrusa.com/c/memory/2015/02/20/memory/) `2/20/2015, Marek Vavrusa`
* [A quick tutorial on implementing and debugging malloc, free, calloc, and realloc](https://danluu.com/malloc-tutorial/) `Dec 2014, Dan Luu`
* [Memory management in C programs](http://nethack4.org/blog/memory.html) `03/16/2014, Alex Smith`
* ![pdf doc](./img/pdf_doc.png?raw=true) [A Malloc Tutorial*](http://www.inf.udec.cl/~leo/Malloc_tutorial.pdf) `2/16/2009, Marwan Burelle`

### Structures and Unions

* [The Lost Art of C Structure Packing](https://www.catb.org/esr/structure-packing/) `last update: 06/01/2017 - Eric S.Raymond`
