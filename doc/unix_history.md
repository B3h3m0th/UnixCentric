# Unix History and timeline

* [The Evolution of the Unix Time-sharing System](https://www.bell-labs.com/usr/dmr/www/hist.html) `Dennis M.Ritchie`
* [A Research UNIX Reader: Annotated Excerpts from the Programmer's Manual, 1971-1986](http://www.cs.dartmouth.edu/~doug/reader.pdf) `M. Douglas McIlroy`
* [History and Timeline](http://www.unix.org/what_is_unix/history_timeline.html)
