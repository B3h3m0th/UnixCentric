# UnixCentric Bibliography

The list include all the books which have been useful for the writer, that
any Unix Developer should read at least once in his or her life.

## Unix Systems Programming
  
* **Advanced Programming in the UNIX Environment**. *Richard Stevens*, *Stephen A.Rago* (`O'Reilly, 2013, 3rd`)
* **The Linux Programming Interface - A Linux and UNIX System Programming Handbook**. *Michael Kerrisk* (`No Starch Press, 2010`)
* **Advanced Unix Programming**. *Marc J.Rochkind* (`2nd, 2004, Addison Wesley`);
* **The Art of UNIX Programming**. *Eric S.Raymond* (`Addison Wesley, 2003`)
* **Unix Systems Programming - Communication, Concurrency, and Threads**. *Kay A.Robbins*, *Steven Robbins* (`Prentice Hall, 2003, 2nd`)
* **The Unix Programming Environment**. *Brian W.Kernighan*, *Rob Pike* (`Prentice Hall, 1983`)

## POSIX

* **Programming with POSIX Threads**. *David B.Butenhof* (`Addison Wesley, 1997`)
* **POSIX.4 - Programmers Guide: Programming for the Real World**. *Bill Gallmeister* (`O'Reilly, 1995`)
* **POSIX Programmers Guide**. *Donald Lewine* (`O'Reilly, 1991`)

## Unix Network Programming

* **Unix Network Programming, Volume 1 - The Sockets Networking API**. *W.Richard Stevens*, *Bill Fenner*, *Andrew M.Rudoff* (`Addison Wesley, 2003, 3rd`)
* **Unix Network Programming, Volume 2 - Interprocess Communications**. *W.Richard Stevens* (`Prentice Hall, 1998, 2nd`)


## Linux Systems Programming

* **Linux System Programming - Talking Directly to the Kernel and C Library**. *Robert Love* (`O'Reilly, 2013, 2nd`)
* **Linux Programming by Example - the Fundamentals**. *Arnold Robbins* (`Prentice Hall, 2004`)

## Linux Kernel and Device Drivers

* **Linux Kernel Development**. *Robert Love* (`Addison Wesley, 2010, 3rd`)
* **Understanding the Linux Kernel - from I/O Ports to Process Management**. *Daniel P.Bovet*, *Marco Cesati* (`O'Reilly, 2005, 3rd`);
* **Linux Device Drivers**. *Jonathan Corbet*, *Alessandro Rubini*, *Greg Kroah-Hartman* (`O'Reilly, 2005, 3rd`)

## BSD Kernel and Device Drivers

* **The Design and Implementation of the FreeBSD Operating System**. *Marshal Kirk McKusick*, *George V. Neville-Nell*, *Robert N.M.Watson* (`Addison Wesley, 2014, 2nd`)
* **FreeBSD Device Drivers - A Guide for the Intrepid**. *Joseph Kong* (`No Starch Press, 2012`)
* **Designing BSD Rootkits - An Introduction to Kernel Hacking**. *Joseph Kong* (`No Starch Press, 2007`)

## Solaris

* **Solaris Internals - Solaris 10 and OpenSolaris Kernel Architecture**. *Richard McDougall*, *Jim Mauro* (`Prentice Hall, 2006, 2nd`)

## Unix Operating Systems (Internals)

* **UNIX Internals - The New Frontiers**. *Uresh Vahalia* (`Pearson, 1996`)
* **The Magic Garden Explained - The Internals of UNIX System V Release 4 an Open Systems Design**. *Berny Goodheart*, *James Cox*, *John R.Mashey* (`Prentice Hall, 1994`)
* **The Design of the UNIX Operating System**. *Maurice J.Bach* (`Prentice Hall, 1986`)
* **Lions' Commentary on Unix - A commentary on the Sixth Edition UNIX Operating System**. *John Lions* (`Peer-to-Peer Communications, 1977`)

## The C Language Programming

### c11 - iso9899:2011 (current)

* **C in a Nutshell**. *Peter Prinz*, *Tony Crawford* (`O'Reilly, 2016, 2nd`)
* **Programming in C**. *Stephen G.Kochan* (`Addison Wesley, 2015, 4th`)
* **21st Century C - C Tips from the New School**. *Ben Klemens* (`O'Reilly, 2014, 2nd`)
* **The CERT C Coding Standard - 98 Rules for Developing Safe, Reliable, and Secure Systems**. *Robert C.Seacord* (`Addison Wesley, 2014`)

### c99 - iso9899:1999
  
* **Secure coding in C and C++**. *Robert C.Seacord* (`Addison Wesley, 2013, 2nd`)
* **C Programming: A modern Approach**. *Kim N.King* (`W. W. Norton & Company, 2008, 2nd`)
* **The Practice of Programming**. *Brian W.Kernighan*, *Rob Pike* (`Addison Wesley, 1999`)

### c89 (a.k.a. c90) - iso9899:1990
  
* **A book on C - Programming in C**. *Al Kelley*, *Ira Pohl* (`Addison Wesley, 1998, 4th`)
* **Pointers on C**. *Kenneth Reek* (`Pearson, 1997`)
* **C Interfaces and Implementations - Techniques for Creating Reusable Software**. *David R.Hanson* (`Addison Wesley, 1996`)
* **Expert C Programming - Deep C Secrets**. *Peter Van Der Linden* (`Prentice Hall, 1994`)
* **The Standard C Library**. *Phillip James Plauger* (`Prentice Hall, 1992`)
* **The C Programming Language - ANSI C Version**. *Brian W.Kernighan* (`Prentice Hall, 1988, 2nd`)
